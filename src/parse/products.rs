use std::borrow::Borrow;
use std::borrow::Cow;

use crate::ParseResult;
use crate::Product;

pub(crate) fn default_parse_products<'a>(
    p_cls: u16,
    products: &'a [&'a Product],
) -> Vec<&'a Product> {
    let mut result = vec![];
    for p in products {
        for b in <Cow<'_, [u16]> as Borrow<[u16]>>::borrow(&p.bitmasks) {
            if b & p_cls != 0 {
                result.push(*p);
            }
        }
    }
    result
}

pub(crate) fn default_parse_product<'a>(
    p_cls: u16,
    products: &'a [&'a Product],
) -> ParseResult<&'a Product> {
    for p in products {
        for b in <Cow<'_, [u16]> as Borrow<[u16]>>::borrow(&p.bitmasks) {
            if p_cls == *b {
                return Ok(p);
            }
        }
    }
    Err(format!("Unknown product bit: {:b}", p_cls).into())
}
