#[cfg(feature = "avv-profile")]
pub mod avv;
#[cfg(feature = "bart-profile")]
pub mod bart;
#[cfg(feature = "bls-profile")]
pub mod bls;
#[cfg(feature = "cfl-profile")]
pub mod cfl;
#[cfg(feature = "cmta-profile")]
pub mod cmta;
#[cfg(feature = "dart-profile")]
pub mod dart;
#[cfg(feature = "db-profile")]
pub mod db;
#[cfg(feature = "db-busradar-nrw-profile")]
pub mod db_busradar_nrw;
#[cfg(feature = "insa-profile")]
pub mod insa;
#[cfg(feature = "irish-rail-profile")]
pub mod irish_rail;
#[cfg(feature = "mobil-nrw-profile")]
pub mod mobil_nrw;
#[cfg(feature = "mobiliteit-lu-profile")]
pub mod mobiliteit_lu;
#[cfg(feature = "nahsh-profile")]
pub mod nahsh;
#[cfg(feature = "nvv-profile")]
pub mod nvv;
#[cfg(feature = "oebb-profile")]
pub mod oebb;
#[cfg(feature = "ooevv-profile")]
pub mod ooevv;
#[cfg(feature = "vgi-profile")]
pub mod vgi;
// Currently disabled due to: Does not answer requests for location query.
// #[cfg(feature = "pkp-profile")]
// pub mod pkp;
#[cfg(feature = "rejseplanen-profile")]
pub mod rejseplanen;
#[cfg(feature = "rmv-profile")]
pub mod rmv;
#[cfg(feature = "rsag-profile")]
pub mod rsag;
#[cfg(feature = "saarvv-profile")]
pub mod saarvv;
#[cfg(feature = "salzburg-profile")]
pub mod salzburg;
#[cfg(feature = "sbahn-muenchen-profile")]
pub mod sbahn_muenchen;
// Currently broken due to: <https://github.com/public-transport/hafas-client/issues/284>
// #[cfg(feature = "sncf-profile")]
// pub mod sncf;
// Currently broken due to: HAFAS Kernel: Date outside of the timetable period.
// #[cfg(feature = "tpg-profile")]
// pub mod tpg;
#[cfg(feature = "svv-profile")]
pub mod svv;
#[cfg(feature = "vbb-profile")]
pub mod vbb;
#[cfg(feature = "vbn-profile")]
pub mod vbn;
#[cfg(feature = "verbundlinie-profile")]
pub mod verbundlinie;
#[cfg(feature = "vkg-profile")]
pub mod vkg;
#[cfg(feature = "vmt-profile")]
pub mod vmt;
#[cfg(feature = "vor-profile")]
pub mod vor;
#[cfg(feature = "vos-profile")]
pub mod vos;
#[cfg(feature = "vrn-profile")]
pub mod vrn;
#[cfg(feature = "vsn-profile")]
pub mod vsn;
#[cfg(feature = "vvt-profile")]
pub mod vvt;
#[cfg(feature = "vvv-profile")]
pub mod vvv;
#[cfg(feature = "zvv-profile")]
pub mod zvv;
// ADD PROFILE HERE

// TODO:
// BVG: Too many special things for now
// SNCB: Custom certificate
// KVB: Custom certificate
// IVB: Custom certificate

use chrono::DateTime;
use chrono::FixedOffset;
use chrono::NaiveDate;
#[cfg(feature = "polylines")]
use geojson::Feature;
use serde_json::Value;
use std::collections::HashMap;

use crate::Journey;
use crate::Leg;
use crate::Line;
use crate::LoadFactor;
use crate::Operator;
use crate::ParseResult;
use crate::Place;
use crate::Product;
use crate::Remark;
use crate::Stopover;
use crate::TariffClass;

use crate::api::journeys::JourneysResponse;
use crate::api::locations::LocationsResponse;

use crate::parse::arrival_or_departure::*;
use crate::parse::common::*;
use crate::parse::date::*;
use crate::parse::journey::*;
use crate::parse::journeys_response::*;
use crate::parse::leg::*;
use crate::parse::line::*;
use crate::parse::load_factor::*;
use crate::parse::location::*;
use crate::parse::locations_response::*;
use crate::parse::operator::*;
#[cfg(feature = "polylines")]
use crate::parse::polyline::*;
use crate::parse::products::*;
use crate::parse::remark::*;
use crate::parse::stopover::*;

pub fn profile_from_name<S: AsRef<str>>(name: S) -> Option<Box<dyn Profile>> {
    match &name.as_ref().to_lowercase()[..] {
        #[cfg(feature = "db-profile")]
        "db" => Some(Box::new(db::DbProfile {})),
        #[cfg(feature = "vbb-profile")]
        "vbb" => Some(Box::new(vbb::VbbProfile {})),
        // #[cfg(feature = "sncf-profile")]
        // "sncf" => Some(Box::new(sncf::SncfProfile {})),
        #[cfg(feature = "oebb-profile")]
        "oebb" => Some(Box::new(oebb::OebbProfile {})),
        #[cfg(feature = "nahsh-profile")]
        "nahsh" => Some(Box::new(nahsh::NahSHProfile {})),
        #[cfg(feature = "vvt-profile")]
        "vvt" => Some(Box::new(vvt::VvtProfile {})),
        // #[cfg(feature = "pkp-profile")]
        // "pkp" => Some(Box::new(pkp::PkpProfile {})),
        #[cfg(feature = "irish-rail-profile")]
        "irish-rail" => Some(Box::new(irish_rail::IrishRailProfile {})),
        #[cfg(feature = "mobiliteit-lu-profile")]
        "mobiliteit-lu" => Some(Box::new(mobiliteit_lu::MobiliteitLuProfile {})),
        #[cfg(feature = "bart-profile")]
        "bart" => Some(Box::new(bart::BartProfile {})),
        #[cfg(feature = "dart-profile")]
        "dart" => Some(Box::new(dart::DartProfile {})),
        #[cfg(feature = "insa-profile")]
        "insa" => Some(Box::new(insa::InsaProfile {})),
        #[cfg(feature = "rmv-profile")]
        "rmv" => Some(Box::new(rmv::RmvProfile {})),
        #[cfg(feature = "cmta-profile")]
        "cmta" => Some(Box::new(cmta::CmtaProfile {})),
        #[cfg(feature = "sbahn-muenchen-profile")]
        "sbahn-muenchen" => Some(Box::new(sbahn_muenchen::SBahnMuenchenProfile {})),
        #[cfg(feature = "saarvv-profile")]
        "saarvv" => Some(Box::new(saarvv::SaarvvProfile {})),
        #[cfg(feature = "saarvv-profile")]
        "saarfahrplan" => Some(Box::new(saarvv::SaarvvProfile {})),
        #[cfg(feature = "svv-profile")]
        "svv" => Some(Box::new(svv::SvvProfile {})),
        #[cfg(feature = "cfl-profile")]
        "cfl" => Some(Box::new(cfl::CflProfile {})),
        #[cfg(feature = "nvv-profile")]
        "nvv" => Some(Box::new(nvv::NvvProfile {})),
        #[cfg(feature = "mobil-nrw-profile")]
        "mobil-nrw" => Some(Box::new(mobil_nrw::MobilNrwProfile {})),
        #[cfg(feature = "db-busradar-nrw-profile")]
        "db-busradar-nrw" => Some(Box::new(db_busradar_nrw::DbBusradarNrwProfile {})),
        #[cfg(feature = "vsn-profile")]
        "vsn" => Some(Box::new(vsn::VsnProfile {})),
        // INVG is the old name for VGI. For backwards compatibility, allow also the old name.
        #[cfg(feature = "vgi-profile")]
        "invg" => Some(Box::new(vgi::VgiProfile {})),
        #[cfg(feature = "vgi-profile")]
        "vgi" => Some(Box::new(vgi::VgiProfile {})),
        #[cfg(feature = "vbn-profile")]
        "vbn" => Some(Box::new(vbn::VbnProfile {})),
        #[cfg(feature = "vrn-profile")]
        "vrn" => Some(Box::new(vrn::VrnProfile {})),
        #[cfg(feature = "rsag-profile")]
        "rsag" => Some(Box::new(rsag::RsagProfile {})),
        #[cfg(feature = "vmt-profile")]
        "vmt" => Some(Box::new(vmt::VmtProfile {})),
        #[cfg(feature = "vos-profile")]
        "vos" => Some(Box::new(vos::VosProfile {})),
        #[cfg(feature = "avv-profile")]
        "avv" => Some(Box::new(avv::AvvProfile {})),
        #[cfg(feature = "rejseplanen-profile")]
        "rejseplanen" => Some(Box::new(rejseplanen::RejseplanenProfile {})),
        #[cfg(feature = "ooevv-profile")]
        "ooevv" => Some(Box::new(ooevv::OoevvProfile {})),
        #[cfg(feature = "salzburg-profile")]
        "salzburg" => Some(Box::new(salzburg::SalzburgProfile {})),
        #[cfg(feature = "verbundlinie-profile")]
        "verbundlinie" => Some(Box::new(verbundlinie::VerbundlinieProfile {})),
        // STV is the old name for Verbundlinie. For backwards compatibility, allow also the old name.
        #[cfg(feature = "verbundlinie-profile")]
        "stv" => Some(Box::new(verbundlinie::VerbundlinieProfile {})),
        #[cfg(feature = "vor-profile")]
        "vor" => Some(Box::new(vor::VorProfile {})),
        #[cfg(feature = "vkg-profile")]
        "vkg" => Some(Box::new(vkg::VkgProfile {})),
        #[cfg(feature = "vvv-profile")]
        "vvv" => Some(Box::new(vvv::VvvProfile {})),
        // #[cfg(feature = "tpg-profile")]
        // "tpg" => Some(Box::new(tpg::TpgProfile {})),
        #[cfg(feature = "bls-profile")]
        "bls" => Some(Box::new(bls::BlsProfile {})),
        // TODO: Does not work, authorization failed.
        #[cfg(feature = "zvv-profile")]
        "zvv" => Some(Box::new(zvv::ZvvProfile {})),
        // ADD MATCH HERE
        _ => None,
    }
}

pub trait Profile: Send + Sync {
    fn url(&self) -> &'static str;
    fn checksum_salt(&self) -> Option<&'static str> {
        None
    }
    fn prepare_body(&self, req_json: &mut Value);
    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>);
    fn price_currency(&self) -> &'static str;
    fn timezone(&self) -> chrono_tz::Tz;
    fn language(&self) -> &'static str {
        "en"
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        false
    }
    fn mic_mac(&self) -> bool {
        false
    }
    fn salt(&self) -> bool {
        false
    }

    fn products(&self) -> &'static [&'static Product];

    fn parse_common(
        &self,
        data: HafasCommon,
        tariff_class: TariffClass,
    ) -> ParseResult<CommonData> {
        default_parse_common(self, data, tariff_class)
    }
    fn parse_arrival_or_departure(
        &self,
        data: HafasArrivalOrDeparture,
        date: &NaiveDate,
    ) -> ParseResult<ArrivalOrDeparture> {
        default_parse_arrival_or_departure(self, data, date)
    }
    fn parse_stopover(
        &self,
        data: HafasStopover,
        common: &CommonData,
        date: &NaiveDate,
    ) -> ParseResult<Stopover> {
        default_parse_stopover(self, data, common, date)
    }
    fn parse_remark(&self, data: HafasRemark) -> ParseResult<Remark> {
        default_parse_remark(data)
    }
    fn parse_products(&self, p_cls: u16) -> Vec<&'static Product> {
        default_parse_products(p_cls, self.products())
    }
    fn parse_product(&self, p_cls: u16) -> ParseResult<&'static Product> {
        default_parse_product(p_cls, self.products())
    }
    #[cfg(feature = "polylines")]
    fn parse_polyline(&self, data: HafasPolyline) -> ParseResult<Vec<Feature>> {
        default_parse_polyline(data)
    }
    fn parse_operator(&self, data: HafasOperator) -> ParseResult<Operator> {
        default_parse_operator(data)
    }
    fn parse_locations_response(
        &self,
        data: HafasLocationsResponse,
    ) -> ParseResult<LocationsResponse> {
        default_parse_locations_response(self, data)
    }
    fn parse_coords(&self, data: HafasCoords) -> (f32, f32) {
        default_parse_coords(data)
    }
    fn parse_place(&self, data: HafasPlace) -> ParseResult<Place> {
        default_parse_place(self, data)
    }
    fn parse_line(&self, data: HafasLine, operators: &[Operator]) -> ParseResult<Line> {
        default_parse_line(self, data, operators)
    }
    fn parse_leg(
        &self,
        data: HafasLeg,
        common: &CommonData,
        date: &NaiveDate,
    ) -> ParseResult<Option<Leg>> {
        default_parse_leg(self, data, common, date)
    }
    fn parse_journeys_response(
        &self,
        data: HafasJourneysResponse,
        tariff_class: TariffClass,
    ) -> ParseResult<JourneysResponse> {
        default_parse_journeys_response(self, data, tariff_class)
    }
    fn parse_date(
        &self,
        time: Option<String>,
        tz_offset: Option<i32>,
        date: &NaiveDate,
    ) -> ParseResult<Option<DateTime<FixedOffset>>> {
        default_parse_date(self, time, tz_offset, date)
    }
    fn parse_load_factor_entry(&self, h: HafasLoadFactorEntry) -> ParseResult<LoadFactorEntry> {
        default_parse_load_factor_entry(self, h)
    }
    fn parse_load_factor(&self, h: HafasLoadFactor) -> ParseResult<LoadFactor> {
        default_parse_load_factor(h)
    }
    fn parse_journey(&self, data: HafasJourney, common: &CommonData) -> ParseResult<Journey> {
        default_parse_journey(self, data, common)
    }
}

impl<T: Profile + ?Sized> Profile for Box<T> {
    fn url(&self) -> &'static str {
        (**self).url()
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        (**self).checksum_salt()
    }
    fn prepare_body(&self, req_json: &mut Value) {
        (**self).prepare_body(req_json)
    }
    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        (**self).prepare_headers(headers)
    }
    fn price_currency(&self) -> &'static str {
        (**self).price_currency()
    }
    fn timezone(&self) -> chrono_tz::Tz {
        (**self).timezone()
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        (**self).refresh_journey_use_out_recon_l()
    }
    fn mic_mac(&self) -> bool {
        (**self).mic_mac()
    }
    fn salt(&self) -> bool {
        (**self).salt()
    }

    fn products(&self) -> &'static [&'static Product] {
        (**self).products()
    }

    fn parse_common(
        &self,
        data: HafasCommon,
        tariff_class: TariffClass,
    ) -> ParseResult<CommonData> {
        (**self).parse_common(data, tariff_class)
    }
    fn parse_arrival_or_departure(
        &self,
        data: HafasArrivalOrDeparture,
        date: &NaiveDate,
    ) -> ParseResult<ArrivalOrDeparture> {
        (**self).parse_arrival_or_departure(data, date)
    }
    fn parse_stopover(
        &self,
        data: HafasStopover,
        common: &CommonData,
        date: &NaiveDate,
    ) -> ParseResult<Stopover> {
        (**self).parse_stopover(data, common, date)
    }
    fn parse_remark(&self, data: HafasRemark) -> ParseResult<Remark> {
        (**self).parse_remark(data)
    }
    fn parse_products(&self, p_cls: u16) -> Vec<&'static Product> {
        (**self).parse_products(p_cls)
    }
    fn parse_product(&self, p_cls: u16) -> ParseResult<&'static Product> {
        (**self).parse_product(p_cls)
    }
    #[cfg(feature = "polylines")]
    fn parse_polyline(&self, data: HafasPolyline) -> ParseResult<Vec<Feature>> {
        (**self).parse_polyline(data)
    }
    fn parse_operator(&self, data: HafasOperator) -> ParseResult<Operator> {
        (**self).parse_operator(data)
    }
    fn parse_locations_response(
        &self,
        data: HafasLocationsResponse,
    ) -> ParseResult<LocationsResponse> {
        (**self).parse_locations_response(data)
    }
    fn parse_coords(&self, data: HafasCoords) -> (f32, f32) {
        (**self).parse_coords(data)
    }
    fn parse_place(&self, data: HafasPlace) -> ParseResult<Place> {
        (**self).parse_place(data)
    }
    fn parse_line(&self, data: HafasLine, operators: &[Operator]) -> ParseResult<Line> {
        (**self).parse_line(data, operators)
    }
    fn parse_leg(
        &self,
        data: HafasLeg,
        common: &CommonData,
        date: &NaiveDate,
    ) -> ParseResult<Option<Leg>> {
        (**self).parse_leg(data, common, date)
    }
    fn parse_journeys_response(
        &self,
        data: HafasJourneysResponse,
        tariff_class: TariffClass,
    ) -> ParseResult<JourneysResponse> {
        (**self).parse_journeys_response(data, tariff_class)
    }
    fn parse_date(
        &self,
        time: Option<String>,
        tz_offset: Option<i32>,
        date: &NaiveDate,
    ) -> ParseResult<Option<DateTime<FixedOffset>>> {
        (**self).parse_date(time, tz_offset, date)
    }
    fn parse_load_factor_entry(&self, h: HafasLoadFactorEntry) -> ParseResult<LoadFactorEntry> {
        (**self).parse_load_factor_entry(h)
    }
    fn parse_load_factor(&self, h: HafasLoadFactor) -> ParseResult<LoadFactor> {
        (**self).parse_load_factor(h)
    }
    fn parse_journey(&self, data: HafasJourney, common: &CommonData) -> ParseResult<Journey> {
        (**self).parse_journey(data, common)
    }
}

#[cfg(test)]
pub mod test {
    use crate::{
        api::{journeys::JourneysOptions, locations::LocationsOptions},
        client::HafasClient,
        requester::hyper::HyperRustlsRequester,
        Location, Place, Profile, Stop,
    };

    pub async fn check_search<S: AsRef<str>, P: Profile + 'static>(
        profile: P,
        search: S,
        expected: S,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let client = HafasClient::new(profile, HyperRustlsRequester::new());
        let locations = client
            .locations(LocationsOptions {
                query: search.as_ref().to_string(),
                ..Default::default()
            })
            .await?;
        let results = locations
            .into_iter()
            .flat_map(|p| match p {
                Place::Stop(s) => s.name,
                Place::Location(Location::Address { address, .. }) => Some(address),
                Place::Location(Location::Point { name, .. }) => name,
            })
            .collect::<Vec<_>>();
        assert!(
            results.iter().find(|s| s == &expected.as_ref()).is_some(),
            "expected {} to be contained in {:#?}",
            expected.as_ref(),
            results
        );
        Ok(())
    }

    pub async fn check_journey<S: AsRef<str>, P: Profile + 'static>(
        profile: P,
        from: S,
        to: S,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let client = HafasClient::new(profile, HyperRustlsRequester::new());
        let journeys = client
            .journeys(
                Place::Stop(Stop {
                    id: from.as_ref().to_string(),
                    ..Default::default()
                }),
                Place::Stop(Stop {
                    id: to.as_ref().to_string(),
                    ..Default::default()
                }),
                JourneysOptions::default(),
            )
            .await?;
        assert!(
            !journeys.journeys.is_empty(),
            "expected journey from {} to {} to exist",
            from.as_ref(),
            to.as_ref()
        );
        Ok(())
    }
}
