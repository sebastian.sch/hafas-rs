#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const S: Product = Product {
        id: Cow::Borrowed("bahn-s-bahn"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[1, 2]),
        name: Cow::Borrowed("Bahn & S-Bahn"),
        short: Cow::Borrowed("S/Zug"),
    };
    pub const U: Product = Product {
        id: Cow::Borrowed("u-bahn"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("U-Bahn"),
        short: Cow::Borrowed("U"),
    };
    pub const STR: Product = Product {
        id: Cow::Borrowed("strassenbahn"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[16]),
        name: Cow::Borrowed("Strassenbahn"),
        short: Cow::Borrowed("Str"),
    };
    pub const FERNBUS: Product = Product {
        id: Cow::Borrowed("fernbus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Fernbus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const REGIONALBUS: Product = Product {
        id: Cow::Borrowed("regionalbus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[64]),
        name: Cow::Borrowed("Regionalbus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const STADTBUS: Product = Product {
        id: Cow::Borrowed("stadtbus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("Stadtbus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const SEIL_: Product = Product {
        id: Cow::Borrowed("seilbahn-zahnradbahn"),
        mode: Mode::Gondola,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("Seil-/Zahnradbahn"),
        short: Cow::Borrowed("Seil-/Zahnradbahn"),
    };
    pub const F: Product = Product {
        id: Cow::Borrowed("schiff"),
        mode: Mode::Watercraft,
        bitmasks: Cow::Borrowed(&[512]),
        name: Cow::Borrowed("Schiff"),
        short: Cow::Borrowed("F"),
    };

    pub const PRODUCTS: &[&Product] =
        &[&S, &U, &STR, &FERNBUS, &REGIONALBUS, &STADTBUS, &SEIL_, &F];
}

#[derive(Debug)]
pub struct SvvProfile;

impl Profile for SvvProfile {
    fn url(&self) -> &'static str {
        "https://fahrplan.salzburg-verkehr.at/bin/mgate.exe"
    }
    fn language(&self) -> &'static str {
        "de"
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Vienna
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        None
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] = json!({"type":"WEB","id":"VAO","v":"","name":"webapp"});
        req_json["ver"] = json!("1.39");
        req_json["ext"] = json!("VAO.11");
        req_json["auth"] = json!({"type":"AID","aid":"wf7mcf9bv3nv8g5f"});
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "EUR"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl SvvProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use std::error::Error;

    use crate::profile::test::{check_journey, check_search};

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(SvvProfile {}, "Sal", "Salzburg Hauptbahnhof").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(SvvProfile {}, "455086100", "455082100").await
    }
}
