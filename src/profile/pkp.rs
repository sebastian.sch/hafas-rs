#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

// TODO: Trim stop name

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const EIC: Product = Product {
        id: Cow::Borrowed("high-speed-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[1, 2]),
        name: Cow::Borrowed("ExpressInterCity & ExpressInterCity Premium & InterCityExpress"),
        short: Cow::Borrowed("EIC/EIP/ICE"),
    };
    pub const IC: Product = Product {
        id: Cow::Borrowed("long-distance-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("InterCity & Twoje Linie Kolejowe & EuroCity & EuroNight"),
        short: Cow::Borrowed("IC/TLK/EC/EN"),
    };
    pub const R: Product = Product {
        id: Cow::Borrowed("regional-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("Regional"),
        short: Cow::Borrowed("R"),
    };
    pub const B: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Bus"),
        short: Cow::Borrowed("B"),
    };

    pub const PRODUCTS: &[&Product] = &[&EIC, &IC, &R, &B];
}

#[derive(Debug)]
pub struct PkpProfile;

impl Profile for PkpProfile {
    fn url(&self) -> &'static str {
        "https://mobil.rozklad-pkp.pl:8019/bin/mgate.exe"
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Warsaw
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] = json!({
            "type": "AND",
            "id": "HAFAS"
        });
        req_json["ver"] = json!("1.21");
        req_json["auth"] = json!({
            "type": "AID",
            "aid": "DrxJYtYZQpEBCtcb"
        });
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "PLN"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl PkpProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use crate::{
        api::journeys::JourneysOptions, client::HafasClient,
        requester::hyper::HyperRustlsRequester, Place, Stop,
    };
    use std::error::Error;

    use super::*;

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        let client = HafasClient::new(PkpProfile {}, HyperRustlsRequester::new());
        let journeys = client
            .journeys(
                Place::Stop(Stop {
                    id: "5100069".to_string(),
                    ..Default::default()
                }),
                Place::Stop(Stop {
                    id: "5100028".to_string(),
                    ..Default::default()
                }),
                JourneysOptions::default(),
            )
            .await?;
        assert!(!journeys.journeys.is_empty());
        Ok(())
    }
}
